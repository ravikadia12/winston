const { createLogger, format, transports } = require('winston');
var winston = require('winston');

require('winston-daily-rotate-file');
const colorizer = winston.format.colorize();

const { combine, timestamp, label, prettyPrint } = winston.format

var transportError = new winston.transports.DailyRotateFile({
    json: false,
    filename: 'logs/%DATE%-logs/server-error.log',
    datePattern: 'DD-MM-YYYY',
    format: combine(timestamp(), prettyPrint()),
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    level: 'error'
  });


  var transportInfo = new winston.transports.DailyRotateFile({
    json: false,
    filename: 'logs/%DATE%-logs/server-info.log',
    datePattern: 'DD-MM-YYYY',
    format: combine(timestamp(), prettyPrint()),
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    level: 'info'
  });


module.exports = winston.createLogger({
    format: format.combine(
        format.timestamp(),
        format.json()
    ),
transports:[
    transportError,
    transportInfo
  ]
});
